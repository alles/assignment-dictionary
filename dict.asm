; rdi - string, rsi - dict word ptr

global find_word
extern string_equals

section .text
find_word: 
    cmp rsi, 0 
    je .not_found 
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 0
    jne .found
    mov rsi, [rsi]
    jmp find_word
.not_found:
    mov rax, 0	
    ret
.found:
    mov rax, rsi
    ret

