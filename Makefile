main: main.o dict.o lib.o
	ld $^ -o $@

main.o: main.asm colon.inc words.inc
	nasm -felf64 -o $@ main.asm

dict.o: dict.asm
	nasm -felf64 -o $@ $^	

lib.o: lib.asm
	nasm -felf64 -o $@ $^	

clean:
	rm -rf *.o
	rm main
