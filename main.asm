%include "words.inc"
extern read_word
extern find_word
extern exit
extern print_string
extern print_newline
extern print_string_err
extern print_newline_err
extern string_length

%define BUFFER_SIZE 256
%define SUCCESS 0
%define ERROR 1

section .rodata:
not_found_msg: db 'Not found', 0
buf_err_msg: db 'Buffer overflow', 0


section .text


global _start

_start:
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
    mov rsi, BUFFER_SIZE
	call read_word

    mov rdi, rax
    mov rsi, next
    call find_word
    cmp rax, 0
	je .not_found
	
	;found
    mov rdi, rax
	add rdi, 8
	call string_length
	add rax, rdi
    add rax, 1 
	mov rdi, rax
	call print_string
	call print_newline
	mov rdi, SUCCESS
	call exit
	

.not_found:
	mov rdi, not_found_msg
	call print_string_err
	call print_newline_err
	mov rdi, ERROR
	call exit
.buff_err:
	mov rdi, buf_err_msg
	call print_string_err
	call print_newline_err
	mov rdi, ERROR
	call exit
