%define next 0
%macro colon 2
    %%some_label: dq next
    db %1, 0
    %2:
    	%define next %%some_label
%endmacro
