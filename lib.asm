section .text
global exit 
global string_length
global print_string
global print_char
global print_newline
global print_string_err
global print_char_err
global print_newline_err
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define stdout 1
%define stderr 2
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
		cmp byte [rdi+rax], 0 
        je .end 
        inc rax 
        jmp .loop 
    .end:
        ret
    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rdx, rax 
    mov rax, 1 
    mov rsi, rdi 
    mov rdi, stdout 
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
	push rdi
    mov rax, 1
    mov rdi, stdout
    mov rsi, rsp 
    mov rdx, 1 
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

print_string_err:
    xor rax, rax
    call string_length
    mov rdx, rax 
    mov rax, 1 
    mov rsi, rdi 
    mov rdi, stderr
    syscall
    ret

print_char_err:
    xor rax, rax
	push rdi
    mov rax, 1
    mov rdi, stderr
    mov rsi, rsp 
    mov rdx, 1 
    syscall
    pop rdi
    ret

print_newline_err:
    xor rax, rax
    mov rdi, 0xA
    call print_char_err
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r8, 10 
    mov rax, rdi 
    mov r9, rsp
    sub rsp, 1;???
    .divide:
      xor rdx, rdx ; чистим rdx
      div r8 ; делим на 10
      add rdx, '0' ; "делаем" символом
      sub rsp, 1
      mov [rsp], dl ; кладём цифру
      test rax, rax ; проверяем на конец
    jnz .divide

    mov rdi, rsp ; печатаем uint (в rdi указатель начала буфера)
    call print_string
    mov rsp, r9; восстановление rsp

    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.positive:   
    call print_uint
    ret  

; Принимает два указателя на нуль-терминированные строки (rdi,rsi), возвращает 1 если они равны, 0 иначе
string_equals:
	mov rax, 1
	mov rcx, 0
	push r8
	push r9
.loop:	
	mov r8b, byte[rdi+rcx]
	mov r9b, byte[rsi+rcx]
	cmp r8b,r9b
	jne .n_eq
	cmp r8b,0
	je .stop
	inc rcx
	jmp .loop
.stop:
	pop r9
	pop r8
    ret
.n_eq:
	mov rax, 0
	pop r9
	pop r8
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
   	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r8
	push r9
	push r10
	mov r8, rdi; начало
	mov r9, rsi; размер
	mov r10, 0; счетчик
.spaces:
	call read_char
	cmp rax, 0     
    je .end      
    cmp rax, 0x20
    je .spaces
    cmp rax, 0x9
    je .spaces
    cmp rax, 0xA
    je .spaces
	
.common_r:
    cmp r9, r10
    je .err
    mov [r8 + r10], rax
    inc r10
	call read_char
	cmp rax, 0
	je .end
	cmp rax, 0x20
    je .end
    cmp rax, 0x9
    je .end
    cmp rax, 0xA
    je .end
    jmp .common_r
	
.err:
	xor rax, rax
	jmp .ret
.end:
	mov qword[r8+r10], 0
	mov rax, r8
	mov rdx, r10
.ret:
	pop r10
	pop r9
	pop r8	
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    push r8
    xor r8, r8
    xor rdx, rdx
    xor rax, rax

.loop:
    cmp byte[rdi + rdx], '0'
    jl .exit
    cmp byte[rdi + rdx], '9'
	jg .exit

    mov r8b, [rdi + rdx] 
    sub r8b, '0' 
    imul rax, 10 
    add rax, r8 
    inc rdx 
    jmp .loop

.exit:
    pop r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte[rdi], '-'
	jne .unsign
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret
.unsign:
    call parse_uint	
    ret 

; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	xor rcx, rcx
	push r8
	
.loop:	
	cmp rdx,rax
	je .err
	mov r8, [rdi]
	mov [rsi], r8
	inc rdi
	inc rsi
	inc rax
	cmp r8, 0
	jne .loop
	pop r8
	ret
	
.err:
	xor rax,rax
	pop r8
    ret
